﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookList.BookListManagement.CloudBookLists
{
    public class CloudBookList:CreationAuditedEntity<long>
    {
        /// <summary>
        /// 书单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 书单简介
        /// </summary>
        public string Info { get; set; }
    }
}
