using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using BookList.Authorization.Roles;
using BookList.Authorization.Users;
using BookList.MultiTenancy;

namespace BookList.EntityFrameworkCore
{
    public class BookListDbContext : AbpZeroDbContext<Tenant, Role, User, BookListDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public BookListDbContext(DbContextOptions<BookListDbContext> options)
            : base(options)
        {
        }
    }
}
